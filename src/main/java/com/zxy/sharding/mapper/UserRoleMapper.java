package com.zxy.sharding.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxy.sharding.model.entity.UserRoleDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author liuyang
 */
@Mapper
public interface UserRoleMapper extends BaseMapper<UserRoleDO> {


}
