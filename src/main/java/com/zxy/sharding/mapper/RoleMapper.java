package com.zxy.sharding.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxy.sharding.model.entity.RoleDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author liuyang
 */
@Mapper
public interface RoleMapper extends BaseMapper<RoleDO> {


}
