package com.zxy.sharding.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zxy.sharding.model.UserDetailResp;
import com.zxy.sharding.model.entity.UserDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author liuyang
 */
@Mapper
public interface UserMapper extends BaseMapper<UserDO> {


    UserDetailResp getUserDetails(@Param("id") Long id);
}
