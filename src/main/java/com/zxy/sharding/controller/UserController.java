package com.zxy.sharding.controller;

import com.zxy.sharding.model.AddUserReq;
import com.zxy.sharding.model.UserDetailResp;
import com.zxy.sharding.service.UserService;
import groovy.util.logging.Slf4j;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * @author liuyang
 */
@Slf4j
@RestController
@RequestMapping("/users")
@Validated
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    /**
     * 添加用户
     *
     * @param req
     */

    @PostMapping("/add")
    public void addUser(@RequestBody @Valid AddUserReq req) {
        userService.addUser(req);
    }


    /**
     * 获取用户信息
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public UserDetailResp getUser(@PathVariable("id") Long id) {
        return userService.getUser(id);
    }

}