package com.zxy.sharding.service;

import com.zxy.sharding.model.AddUserReq;
import com.zxy.sharding.model.UserDetailResp;

/**
 * @program: sharding-demo
 * @description:
 * @author: zhangxiaoyang
 * @date: 2023/3/4 22:31
 **/
public interface UserService {
    void addUser(AddUserReq req);

    UserDetailResp getUser(Long id);
}
