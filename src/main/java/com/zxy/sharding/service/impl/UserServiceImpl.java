package com.zxy.sharding.service.impl;

import com.zxy.sharding.mapper.UserMapper;
import com.zxy.sharding.model.AddUserReq;
import com.zxy.sharding.model.UserDetailResp;
import com.zxy.sharding.model.entity.UserDO;
import com.zxy.sharding.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @program: sharding-demo
 * @description:
 * @author: zhangxiaoyang
 * @date: 2023/3/4 22:32
 **/
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    public final UserMapper userMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addUser(AddUserReq req) {
        UserDO userDO = userMapper.selectById(1L);
        UserDO entity = new UserDO();
        entity.setName(req.getName());
        entity.setBirthday(req.getBirthday());
        userMapper.insert(entity);
    }

    @Override
    public UserDetailResp getUser(Long id) {
        return userMapper.getUserDetails(id);
    }
}
