package com.zxy.sharding.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @program: sharding-demo
 * @description:
 * @author: zhangxiaoyang
 * @date: 2023/3/4 22:39
 **/
@Getter
@Setter
@ToString
public class RoleResp {
    private Integer id;
    private String name;
}
