package com.zxy.sharding.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * @program: sharding-demo
 * @description:
 * @author: zhangxiaoyang
 * @date: 2023/3/4 22:35
 **/
@Getter
@Setter
@ToString
public class AddUserReq {
    private Integer id;
    private String name;
    @JsonFormat(pattern = "yyyy-MM-dd", locale = "zh", timezone = "GMT+8")
    private Date birthday;
}
