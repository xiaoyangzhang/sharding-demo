package com.zxy.sharding.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @program: sharding-demo
 * @description:
 * @author: zhangxiaoyang
 * @date: 2023/3/4 22:29
 **/
@Data
@TableName("role")
public class RoleDO extends  BaseEntity{
    private String name;
}
