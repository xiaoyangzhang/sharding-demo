package com.zxy.sharding.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;

/**
 * @author liuyang
 * @date 2021/7/6
 */
@Getter
@Setter
@ToString
public abstract class BaseEntity {

    @TableId(type = IdType.ASSIGN_ID)
    protected Long id;

    @TableField("create_time")
    protected Date createTime;

    @TableField("update_time")
    protected Date updateTime;

}
