package com.zxy.sharding.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * @program: sharding-demo
 * @description:
 * @author: zhangxiaoyang
 * @date: 2023/3/4 22:27
 **/
@Data
@TableName("user")
public class UserDO extends BaseEntity {
    private String name;
    private Date birthday;
}
