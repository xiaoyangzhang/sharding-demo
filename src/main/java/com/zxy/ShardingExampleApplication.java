package com.zxy;

import com.alibaba.nacos.spring.context.annotation.config.EnableNacosConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 */
@SpringBootApplication
@EnableNacosConfig
public class ShardingExampleApplication {


    public static void main(String[] args) {
        try {
            SpringApplication.run(ShardingExampleApplication.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
