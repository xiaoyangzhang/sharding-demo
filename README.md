# MySQL集群搭建（Docker）
## [安装Docker](https://www.runoob.com/docker/docker-tutorial.html)
## [主从配置](https://juejin.cn/post/7203533641914679351)
# shardingSphere配置信息
spring.shardingsphere.datasource.names=write-ds,read-ds-0,read-ds-1

spring.shardingsphere.datasource.write-ds.url=jdbc:mysql://127.0.0.1:3310/sharding_test?useUnicode=true&characterEncoding=utf-8&autoReconnect=true&autoReconnectForPools=true&useSSL=false&allowMultiQueries=true
spring.shardingsphere.datasource.write-ds.type=com.alibaba.druid.pool.DruidDataSource
spring.shardingsphere.datasource.write-ds.driver-class-name=com.mysql.jdbc.Driver
spring.shardingsphere.datasource.write-ds.dataSourceClassName=com.alibaba.druid.pool.DruidDataSource
spring.shardingsphere.datasource.write-ds.username=root
spring.shardingsphere.datasource.write-ds.password=root

spring.shardingsphere.datasource.read-ds-0.url=jdbc:mysql://127.0.0.1:3311/sharding_test?useUnicode=true&characterEncoding=utf-8&autoReconnect=true&autoReconnectForPools=true&useSSL=false&allowMultiQueries=true
spring.shardingsphere.datasource.read-ds-0.type=com.alibaba.druid.pool.DruidDataSource
spring.shardingsphere.datasource.read-ds-0.driver-class-name=com.mysql.jdbc.Driver
spring.shardingsphere.datasource.read-ds-0.dataSourceClassName=com.alibaba.druid.pool.DruidDataSource
spring.shardingsphere.datasource.read-ds-0.username=slave
spring.shardingsphere.datasource.read-ds-0.password=123456

spring.shardingsphere.datasource.read-ds-1.url=jdbc:mysql://127.0.0.1:3312/sharding_test?useUnicode=true&characterEncoding=utf-8&autoReconnect=true&autoReconnectForPools=true&useSSL=false&allowMultiQueries=true
spring.shardingsphere.datasource.read-ds-1.type=com.alibaba.druid.pool.DruidDataSource
spring.shardingsphere.datasource.read-ds-1.dataSourceClassName=com.alibaba.druid.pool.DruidDataSource
spring.shardingsphere.datasource.read-ds-1.driver-class-name=com.mysql.jdbc.Driver
spring.shardingsphere.datasource.read-ds-1.username=slave
spring.shardingsphere.datasource.read-ds-1.password=123456

#spring.shardingsphere.datasource.read-ds-4.url=jdbc:mysql://127.0.0.1:3313/sharding_test?useUnicode=true&characterEncoding=utf-8&autoReconnect=true&autoReconnectForPools=true&useSSL=false&allowMultiQueries=true
#spring.shardingsphere.datasource.read-ds-4.type=com.alibaba.druid.pool.DruidDataSource
#spring.shardingsphere.datasource.read-ds-4.dataSourceClassName=com.alibaba.druid.pool.DruidDataSource
#spring.shardingsphere.datasource.read-ds-4.driver-class-name=com.mysql.jdbc.Driver
#spring.shardingsphere.datasource.read-ds-4.username=root
#spring.shardingsphere.datasource.read-ds-4.password=root

##读写分离  5.2.x及以上
spring.shardingsphere.database.name=readwrite_ds
spring.shardingsphere.rules.readwrite-splitting.data-sources.readwrite_ds.static-strategy.write-data-source-name=write-ds
spring.shardingsphere.rules.readwrite-splitting.data-sources.readwrite_ds.static-strategy.read-data-source-names=read-ds-0,read-ds-1

spring.shardingsphere.rules.readwrite-splitting.data-sources.readwrite_ds.load-balancer-name=round_robin
spring.shardingsphere.rules.readwrite-splitting.load-balancers.round_robin.type=ROUND_ROBIN
##打印sql
spring.shardingsphere.props.sql-show=true
#spring.shardingsphere.props.sql-simple=true